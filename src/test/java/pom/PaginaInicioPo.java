package pom;

import com.sun.deploy.cache.BaseLocalApplicationProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.sun.activation.registries.LogSupport.log;

public class PaginaInicioPo extends BasePage {



    public PaginaInicioPo(WebDriver driver)
    {
        super(driver);
    }

    @FindBy(xpath = "//body/div[1]/div[3]/form[1]/div[1]/div[1]/div[1]/div[1]/div[2]/input[1]")

    WebElement Search;

    @FindBy(xpath = "/html/head/title")
    WebElement nameGoogle;

    @FindBy(css = "#wp-tabs-container > div:nth-child(3) > div.K20DDe.HdaFGf.suApsd.JXFbbc.Xbg0sb.ySjHwc > div.a1vOw.i49rzb > div > div > div > div.EGmpye > div")
    WebElement firstResult;

    String url = "https://www.google.com/?gws_rd=ssl";





public void start(){
        log("iniciando el test");
        driver.get(url);
        waitFor(3);
}



    public void searchText() {
        waitFor(10);
        Search.click();
        Search.sendKeys("ema");
        Search.submit();
        waitFor(5);
    }

    public String FechaActual() {
        Date fecha = new Date();
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/YYYY");
        return formatoFecha.format(fecha);

    }



    private WriteExcelFile writeFile;


    public void capturoExcel() throws IOException {

        writeFile = new WriteExcelFile();

        waitFor(3);
        firstResult.isSelected();
        String resultText = firstResult.getText();
        log("el numero de orden es" + resultText);
        waitFor(3);
        String filepath = "/Users/alvaroriquelme/IdeaProjects/TestGoogle/test.xlsx";
        System.out.println("Page result text:" + resultText);

        writeFile.writeCellValue(filepath, "Hoja1", 2, 2, resultText);
        writeFile.writeCellValue(filepath, "Hoja1", 2, 3, FechaActual());
        waitFor(3);

    }

}